<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * License page - Licenses display and selection
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

require 'init.php';

use lib\License\LicenseRepository;
use lib\Customer\CustomerRepository;
use lib\Website\WebsiteRepository;
use lib\Website\Website;

$customerRepository = CustomerRepository::Instance();
//$websiteRepository = WebsiteRepository::Instance();
$licenseRepository = LicenseRepository::Instance();

$licenses = $licenseRepository->getAll();

if ($customerId) {
    $customer = $customerRepository->getById($customerId);
    $customerLicense = $customer->getLicense();
} else {
    header("Location: index.php");
    die();
}
$licenseId = filter_input(INPUT_GET, 'licenseId', FILTER_SANITIZE_NUMBER_INT);
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);

if ($licenseId && !is_null($licenseId)) {
    if ($action && !is_null($action) && $action === 'buy') {
        $customer->setLicense($licenseId);
        $customerRepository->update($customer);
        header("Location: main.php");
        die();
    }
}

require 'layout/header.php'; ?>


    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Pricing</h1>
        <p class="lead">Quickly build an effective pricing table for your potential customers with this Bootstrap
            example.
            It’s built with default Bootstrap components and utilities with little customization.</p>
    </div>

    <div class="container">
    <div class="card-deck mb-3 text-center">

        <?php foreach ($licenses as $license) {
            $websiteAllowance = $license->getWebsiteAllowance();
            ?>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal text-uppercase"><?php echo $license->getName(); ?></h4>
                </div>
                <div class="card-body">
                    <div class="plan-icon">
                        <?php if ($websiteAllowance === 0) { ?>
                            <i class="fas fa-infinity"></i>
                        <?php } else {
                            for ($i = 0; $i < $websiteAllowance; $i++) { ?>
                                <i class="fas fa-star"></i>
                            <?php }
                        } ?>
                    </div>
                    <h1 class="card-title pricing-card-title">$<?php echo $license->getPrice(); ?></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li><strong>1 year</strong> of support and updates</li>
                        <li>for <strong>
                                <?php
                                if ($websiteAllowance === 0) {
                                    echo 'unlimited websites';
                                } else if ($websiteAllowance === 1) {
                                    echo $websiteAllowance . " website";
                                } else {
                                    echo $websiteAllowance . " websites";
                                }
                                ?>
                            </strong></li>
                    </ul>
                    <a class="text-decoration-none text-reset"
                       href="license.php?action=buy&licenseId=<?php echo $license->getId(); ?>">
                        <button type="button" class="btn btn-lg btn-block btn-primary"
                            <?php echo ($customerLicense && $customerLicense->getId() === $license->getId()) ? 'disabled' : ''; ?>>
                            BUY WP ROCKET
                        </button>
                    </a>
                </div>
            </div>
        <?php } ?>
    </div>
<?php require 'layout/footer.php'; ?>