<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * Main page - Main user page with current license and websites
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

require 'init.php';

use lib\License\LicenseRepository;
use lib\Customer\CustomerRepository;

if (filter_input(INPUT_GET, 'user', FILTER_SANITIZE_NUMBER_INT)) {
    $customerId = filter_input(INPUT_GET, 'user', FILTER_SANITIZE_NUMBER_INT);
    $_SESSION['customerId'] = $customerId;
}


$customerRepository = CustomerRepository::Instance();
if ($customerId) {
    $customer = $customerRepository->getById($customerId);
    $customerLicense = $customer->getLicense();
    if (!$customerLicense) {
        header("Location: license.php");
        die();
    }
    $websites = $customer->getWebsites();
    $licenseExpiration = $customer->getLicenseExpiration();
} else {
    header("Location: index.php");
    die();
}


require 'layout/header.php'; ?>
<div class="container">
    <div class="card-deck">

        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?php echo $customer->getName(); ?></h5>
                <p class="card-text"><?php echo $customer->getEmail(); ?></p>
                <?php if ($customerLicense) {
                    $websiteAllowance = $customerLicense->getWebsiteAllowance();
                    ?>
                    <div class="card mb-4 shadow-sm text-center">
                        <div class="card-header">
                            <h4 class="my-0 font-weight-normal text-uppercase">
                                <?php echo $customerLicense->getName(); ?>
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="plan-icon">
                                <?php if ($websiteAllowance === 0) { ?>
                                    <i class="fas fa-infinity"></i>
                                <?php } else {
                                    for ($i = 0; $i < $websiteAllowance; $i++) { ?>
                                        <i class="fas fa-star"></i>
                                    <?php }
                                } ?>
                            </div>
                            <h1 class="card-title pricing-card-title">
                                $<?php echo $customerLicense->getPrice(); ?>
                            </h1>
                            <ul class="list-unstyled mt-3 mb-4">
                                <li><strong>1 year</strong> of support and updates</li>
                                <li>for <strong>
                                        <?php
                                        if ($websiteAllowance === 0) {
                                            echo 'unlimited websites';
                                        } else if ($websiteAllowance === 1) {
                                            echo $websiteAllowance . " website";
                                        } else {
                                            echo $websiteAllowance . " websites";
                                        }
                                        ?>
                                    </strong></li>
                                <li>Valid until: <?php echo $licenseExpiration; ?></li>
                            </ul>
                        </div>
                        <div class="card-footer">
                            <a href="license.php" class="btn btn-primary">Switch Plan</a>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Websites
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><h2>Websites</h2></li>

                    <?php foreach ($websites as $website) { ?>
                        <li class="list-group-item"><?php echo $website->getUrl(); ?>
                            <spam class="text-right">
                                <button type="button" class="close" aria-label="Close"
                                        data-href="/websites.php?action=delete&website=<?php echo $website->getId(); ?>"
                                        data-toggle="modal" data-target="#confirm-delete">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </spam>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <footer class="card-footer">
                <form class="form-inline" action="/websites.php" method="post">
                    <input type="hidden" name="action" value="add"/>
                    <div class="form-group w-75">
                        <label for="url" class="sr-only">Website</label>
                        <input type="text" name="url" class="form-control w-100" id="url" placeholder="website"
                            <?php if ($websiteAllowance !== 0 && $websiteAllowance <= count($websites)) {
                                echo 'disabled';
                            }
                            ?>>
                    </div>

                    <button type="submit" class="btn btn-primary ml-3"
                        <?php if ($websiteAllowance !== 0 && $websiteAllowance <= count($websites)) {
                            echo 'disabled';
                        }
                        ?>>
                        Add
                    </button>
                </form>
            </footer>
        </div>
    </div>
</div>
<!--    Confirm delete popup-->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Confirm delete
            </div>
            <div class="modal-body">
                Are you sure you want to delete a website?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancel
                </button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>

<?php require 'layout/footer.php'; ?>
<script>
    $(window).on('load', function () {
        // code here
    });
    $(window).bind("load", function () {
        $('#confirm-delete').on('show.bs.modal', function (e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    });

</script>
