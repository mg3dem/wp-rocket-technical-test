<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * Website entity
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

namespace lib\Website;


/**
 * Class Website
 *
 * @package lib\Website
 */
class Website
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var int
     */
    private $customer;

    /**
     * Website constructor.
     *
     * @param string|null $url
     * @param int|null    $customer
     */
    public function __construct($url = null, $customer = null)
    {
        if (!is_null($url)) {
            $this->url = $url;
        }
        if (!is_null($customer)) {
            $this->customer = $customer;
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getCustomer(): int
    {
        return $this->customer;
    }

    /**
     * @param int $customer
     */
    public function setCustomer(int $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return array
     */
    public function toArray() {
        return array_filter(get_object_vars($this));
    }
}