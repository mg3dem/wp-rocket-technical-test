<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * Website Repository - Websites entities collection
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

namespace lib\Website;

use lib\Database;
use lib\Website\Website;

/**
 * Class WebsiteRepository
 *
 * @package lib\Website
 */
class WebsiteRepository
{
    /**
     * @var
     */
    private static $instance;

    /**
     * @var Database
     */
    private $db;

    /**
     * @return WebsiteRepository
     */
    public static function Instance()
    {
        if (self::$instance !== null) {
            return self::$instance;
        }
        return self::$instance = new WebsiteRepository(Database::Instance());

    }

    /**
     * WebsiteRepository constructor.
     * @param $db
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    /**
     * Get a Website by website id
     *
     * @param int $websiteId
     *
     * @return \lib\Website\Website
     */
    public function getById(int $websiteId): Website
    {
        $statement = $this->db->prepare("SELECT id, url, customer FROM Websites where id=:websiteId");
        $statement->execute(['websiteId' => $websiteId]);
        $statement->setFetchMode(\PDO::FETCH_CLASS, "lib\Website\Website");
        return $statement->fetch();
    }

    /**
     * Get Websites by user id
     *
     * @param int $userId
     *
     * @return array
     */
    public function getByUserId(int $userId): array
    {
        $statement = $this->db->prepare("SELECT id, url, customer FROM Websites where customer=:customerId");
        $statement->execute(['customerId' => $userId]);
        return $statement->fetchAll(\PDO::FETCH_CLASS, "lib\Website\Website");
    }

    /**
     * Remove a website
     *
     * @param \lib\Website\Website $website
     *
     * @return bool
     */
    public function remove(Website $website): bool
    {
        return $this->db->prepare('DELETE FROM Websites WHERE id=:id')->execute(['id' => $website->getId()]);
    }

    /**
     * Add a new website
     *
     * @param \lib\Website\Website $website
     *
     * @return bool
     */
    public function save(Website $website): bool
    {
        return $this->db->prepare('INSERT INTO Websites SET url=:url, customer=:customer ')->execute($website->toArray());
    }
}