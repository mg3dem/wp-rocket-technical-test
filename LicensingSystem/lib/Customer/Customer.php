<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * Customer entity
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

namespace lib\Customer;

use lib\License\License;
use lib\License\LicenseRepository;
use lib\Website\WebsiteRepository;
use \DateTime;
use \DateInterval;

/**
 * Customer class
 * @package lib\Customer
 */
class Customer
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $email;

    /**
     * @var int
     */
    private $license;

    /**
     * @var string
     */
    private $licenseExpiration;

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Customer
     */
    public function setName(string $name): Customer
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $password
     * @return Customer
     */
    public function setPassword(string $password): Customer
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param  string $email
     * @return Customer
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Set License and license expiration
     *
     * @param int $licenseId
     *
     * @return void
     */
    public function setLicense(int $licenseId): void
    {
        $this->license = $licenseId;

        $expiration = new DateTime();
        $expiration->add(new DateInterval("P1Y"));
        $this->licenseExpiration = $expiration->format('Y-m-d H:i:s');
    }

    /**
     * Get Customer's License
     *
     * @return License
     */
    public function getLicense(): License
    {
        $licenseRepository = LicenseRepository::Instance();
        return $licenseRepository->getById($this->license);
    }

    /**
     * Get License expiration datetime
     *
     * @return string
     */
    public function getLicenseExpiration(): string
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $this->licenseExpiration);
        return $date->format('l j F Y');
    }

    /**
     * @return array
     */
    public function getWebsites(): array
    {
        $websiteRepository = WebsiteRepository::Instance();
        return $websiteRepository->getByUserId($this->getId());
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_filter(get_object_vars($this));
    }
}