<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * Customer Repository - Customer's entities collection
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

namespace lib\Customer;

use lib\Database;
use lib\Customer\Customer;

/**
 * CustomerRepository class
 * @package lib\Customer
 */
class CustomerRepository
{
    /**
     * @var
     */
    private static $instance;

    /**
     * @var Database
     */
    private $db;

    /**
     * @return CustomerRepository
     */
    public static function Instance()
    {
        if (self::$instance !== null) {
            return self::$instance;
        }
        return self::$instance = new CustomerRepository(Database::Instance());

    }

    /**
     * CustomerRepository constructor.
     * @param $db
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }


    /**
     * Get all customers
     *
     * @return array
     */
    public function getAll(): array
    {
        $statement = $this->db->prepare("SELECT id, name, email, license, licenseExpiration FROM Customers");
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_CLASS, "lib\Customer\Customer");
    }

    /**
     * Get a Customer by customer id
     *
     * @param  int $userId
     * @return \lib\Customer\Customer
     */
    public function getById(int $userId): Customer
    {
        $statement = $this->db->prepare("SELECT id, name, email, license, licenseExpiration FROM Customers where id=:userId");
        $statement->execute(['userId' => $userId]);
        $statement->setFetchMode(\PDO::FETCH_CLASS, "lib\Customer\Customer");
        return $statement->fetch();
    }

    /**
     * Update Customer
     *
     * @param \lib\Customer\Customer $customer
     * @return bool
     */
    public function update(Customer $customer): bool
    {
        return $this->db->prepare(
            'UPDATE Customers SET id=:id, name=:name, email=:email, license=:license, licenseExpiration=:licenseExpiration  '
        )->execute($customer->toArray());
    }

}