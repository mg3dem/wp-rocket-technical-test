<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * License entity
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

namespace lib\License;

/**
 * Class License
 *
 * @package lib\License
 */
class License
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * @var int
     */
    private $websiteAllowance;


    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName():string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice():float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getWebsiteAllowance():int
    {
        return $this->websiteAllowance;
    }

    /**
     * @param int $websiteAllowance
     */
    public function setWebsiteAllowance(int $websiteAllowance)
    {
        $this->websiteAllowance = $websiteAllowance;
    }

}