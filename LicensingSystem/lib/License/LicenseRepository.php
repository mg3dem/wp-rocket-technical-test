<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * License Repository - Licenses entities collection
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

namespace lib\License;

use lib\Database;
use lib\License\License;

/**
 * Class LicenseRepository
 * @package lib\License
 */
class LicenseRepository
{
    /**
     * @var
     */
    private static $instance;

    /**
     * @var Database
     */
    private $db;

    /**
     * @return LicenseRepository
     */
    public static function Instance(): LicenseRepository
    {
        if (self::$instance !== null) {
            return self::$instance;
        }
        return self::$instance = new LicenseRepository(Database::Instance());

    }

    /**
     * LicenseRepository constructor.
     * @param $db
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    /**
     * Get all Licenses
     *
     * @return array
     */
    public function getAll(): array
    {
        $statement = $this->db->prepare("SELECT id, name, price, websiteAllowance FROM Licenses");
        $statement->execute();
        $statement->setFetchMode(\PDO::FETCH_CLASS, "lib\License\License");
        return $statement->fetchAll();
    }

    /**
     * Get a License by License id
     *
     * @param int $licenseId
     *
     * @return License
     */
    public function getById(int $licenseId): License
    {
        $statement = $this->db->prepare("SELECT id, name, price, websiteAllowance FROM Licenses where id=:licenseId");
        $statement->execute(['licenseId' => $licenseId]);
        $statement->setFetchMode(\PDO::FETCH_CLASS, "lib\License\License");
        return $statement->fetch();
    }
}