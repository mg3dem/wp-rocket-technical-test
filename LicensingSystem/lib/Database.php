<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * Database abstraction to make possible to have only one connection to the db
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

namespace lib;


/**
 * Class Database
 *
 * @package lib
 */
class Database extends \PDO
{
    /**
     * @var
     */
    private static $instance;

    /**
     * @var array|bool
     */
    private $config;

    /**
     * @return Database
     */
    public static function Instance()
    {
        if (self::$instance !== null) {
            return self::$instance;
        }
        return self::$instance = new Database();

    }

    /**
     * Database constructor.
     */
    public function __construct()
    {
        //TODO copy ini file to upper folder from setup file after creating the db and tables
        $this->config = parse_ini_file('/var/www/config.ini');

        $dsn = "mysql:host=" . $this->config['host'] . ";dbname=" . $this->config['dbname'] . ";charset=utf8mb4";

        parent::__construct($dsn, $this->config['username'], $this->config['password']);
    }
}