# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.37-MariaDB-1~bionic)
# Database: technical-test
# Generation Time: 2019-02-06 08:23:01 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE technical-test;
USE technical-test;

# Dump of table Customers
# ------------------------------------------------------------


DROP TABLE IF EXISTS `Customers`;

CREATE TABLE `Customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `license` int(11) DEFAULT NULL,
  `licenseExpiration` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Customers_fk0` (`license`),
  CONSTRAINT `Customers_fk0` FOREIGN KEY (`license`) REFERENCES `Licenses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Customers` WRITE;
/*!40000 ALTER TABLE `Customers` DISABLE KEYS */;

INSERT INTO `Customers` (`id`, `name`, `password`, `email`, `license`, `licenseExpiration`)
VALUES
	(3,'Charles Fleming','$2y$10$EdYqs3zWXOU2ggLwxNeQn.k4N0ch1F1Qmf7vwsmwwwkp4M8vCOxwq','charles.fleming76@example.com',1,'2020-02-06 08:09:15'),
	(4,'Darlene Torres','$2y$10$P.tW9iZJUci0fFMlHJlvKOc3qocDxNJy/iChXG6XX45R.5OIOBrbu','darlene.torres24@example.com',NULL,NULL),
	(5,'Oscar Scott','$2y$10$tPoVHgYYvBmrwBi1e960huLetJETYwuSNR9N3f9oAthDMF5FpKCau','oscar.scott12@example.com',NULL,NULL),
	(6,'Pauline Mckinney','$2y$10$Lkf5MpzpKaRwI/AS.RR.q.4s1kOS4r1mxIn.l5JqLFNtfJczKtc4q','pauline.mckinney97@example.com',NULL,NULL),
	(7,'Jessie Perkins','$2y$10$vyx6QDsvtZh7Le9xNWHdEOEkhjRteajRZGZgzWLVS6kvYT1DEBUJq','jessie.perkins90@example.com',NULL,NULL);

/*!40000 ALTER TABLE `Customers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Licenses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Licenses`;

CREATE TABLE `Licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `websiteAllowance` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Licenses` WRITE;
/*!40000 ALTER TABLE `Licenses` DISABLE KEYS */;

INSERT INTO `Licenses` (`id`, `name`, `price`, `websiteAllowance`)
VALUES
	(1,'Single',49,1),
	(2,'Plus',99,3),
	(3,'Infinite',199,0);

/*!40000 ALTER TABLE `Licenses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Websites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Websites`;

CREATE TABLE `Websites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) DEFAULT NULL,
  `customer` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Websites_fk0` (`customer`),
  CONSTRAINT `Websites_fk0` FOREIGN KEY (`customer`) REFERENCES `Customers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Websites` WRITE;
/*!40000 ALTER TABLE `Websites` DISABLE KEYS */;

INSERT INTO `Websites` (`id`, `url`, `customer`)
VALUES
	(6,'http://www.google.com',3),
	(7,'website',3),
	(8,'yahoo',3),
	(9,'test',3);

/*!40000 ALTER TABLE `Websites` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

