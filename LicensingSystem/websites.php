<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * Websites page - Website's processing
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

require 'init.php';

use lib\License\LicenseRepository;
use lib\Customer\CustomerRepository;
use lib\Website\WebsiteRepository;
use lib\Website\Website;


$customerRepository = CustomerRepository::Instance();
$websiteRepository = WebsiteRepository::Instance();
if ($customerId) {
    $customer = $customerRepository->getById($customerId);
}

if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST') {
    $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
    $url = filter_input(INPUT_POST, 'url', FILTER_SANITIZE_STRING);
} else {
    $websiteId = filter_input(INPUT_GET, 'website', FILTER_SANITIZE_NUMBER_INT);
    $action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
}
switch ($action) {
    case 'delete':
        $website = $websiteRepository->getById($websiteId);

        if ($website->getCustomer() === $customer->getId()) {
            $websiteRepository->remove($website);
        }
        break;
    case 'add':
        $customerLicense = $customer->getLicense();
        $websites = $customer->getWebsites();
        $website = new Website($url, $customerId);
        if ($customerLicense->getWebsiteAllowance() === 0 || $customerLicense->getWebsiteAllowance() > count($websites)) {
            $websiteRepository->save($website);
        }
        break;
}

header("Location: main.php");
die();