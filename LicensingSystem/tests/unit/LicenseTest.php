<?php
use \lib\License\License;

class LicenseTest extends \Codeception\Test\Unit
{
    // tests
    public function testObject()
    {
        $faker = Faker\Factory::create();
        $license = new License();

        $name = $faker->name;
        $license->setName($name);
        $this->assertEquals($name, $license->getName());

        $price = $faker->randomFloat(2);
        $license->setPrice($price);
        $this->assertEquals($price, $license->getPrice());

        $websiteAllowance = $faker->randomDigitNotNull();
        $license->setWebsiteAllowance($websiteAllowance);
        $this->assertEquals($websiteAllowance, $license->getWebsiteAllowance());
    }
}