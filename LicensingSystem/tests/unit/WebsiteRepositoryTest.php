<?php

use \lib\Website\Website;
use \lib\Website\WebsiteRepository;
use Mockery as m;
class WebsiteRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;


    // tests
    public function testGetById()
    {
        $faker = Faker\Factory::create();

        $website = new Website();
        $website->setUrl($faker->url);
        $website->setCustomer($faker->randomDigitNotNull());


        $dbMock = m::mock('\Lib\Database')->makePartial();
        $stMock = m::mock('PDOStatement')->makePartial();
        $stMock->shouldReceive('execute');
        $stMock->shouldReceive('fetch')->andReturn($website);
        $dbMock->shouldReceive('prepare')->andReturn($stMock);

        $websiteRepository = new WebsiteRepository($dbMock);

        $this->assertEquals($website, $websiteRepository->getById($faker->randomDigitNotNull()));
    }

    public function testGetByUserId()
    {
        $faker = Faker\Factory::create();

        $website = new Website();
        $website->setUrl($faker->url);
        $website->setCustomer($faker->randomDigitNotNull());


        $dbMock = m::mock('\Lib\Database')->makePartial();
        $stMock = m::mock('PDOStatement')->makePartial();
        $stMock->shouldReceive('execute');
        $stMock->shouldReceive('fetchAll')->andReturn([$website]);
        $dbMock->shouldReceive('prepare')->andReturn($stMock);

        $websiteRepository = new WebsiteRepository($dbMock);

        $this->assertEquals([$website], $websiteRepository->getByUserId($faker->randomDigitNotNull()));
    }

    public function testRemove()
    {
        $faker = Faker\Factory::create();

        $websiteId = $faker->randomDigitNotNull();
        $website = new Website();
        $website->setUrl($faker->url);
        $website->setCustomer($faker->randomDigitNotNull());

        $reflector = new \ReflectionClass($website);
        $reflector_property = $reflector->getProperty('id');
        $reflector_property->setAccessible(true);

        $reflector_property->setValue($website, $websiteId);

        $dbMock = m::mock('\Lib\Database')->makePartial();
        $stMock = m::mock('PDOStatement')->makePartial();
        $dbMock->shouldReceive('prepare')->andReturn($stMock);
        $stMock->shouldReceive('execute')->with(['id' => $websiteId])->andReturnTrue();

        $websiteRepository = new WebsiteRepository($dbMock);
        $websiteRepository->remove($website);
    }

    public function testSave()
    {
        $faker = Faker\Factory::create();

        $website = new Website();
        $website->setUrl($faker->url);
        $website->setCustomer($faker->randomDigitNotNull());

        $dbMock = m::mock('\Lib\Database')->makePartial();
        $stMock = m::mock('PDOStatement')->makePartial();
        $dbMock->shouldReceive('prepare')->andReturn($stMock);
        $stMock->shouldReceive('execute')->with($website->toArray())->andReturnTrue();

        $websiteRepository = new WebsiteRepository($dbMock);
        $websiteRepository->save($website);
    }
}