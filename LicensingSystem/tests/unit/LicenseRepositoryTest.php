<?php

use lib\Database;
use \lib\License\License;
use \lib\License\LicenseRepository;
use Mockery as m;


class LicenseRepositoryTest extends \Codeception\Test\Unit
{


    protected function _before()
    {

    }
    // tests
    public function testGetAll()
    {
        $faker = Faker\Factory::create();

        $license = new License();
        $license->setName($faker->name);
        $license->setPrice($faker->randomFloat(2));
        $license->setWebsiteAllowance($faker->randomDigitNotNull());


        $dbMock = m::mock('\Lib\Database')->makePartial();
        $stMock = m::mock('PDOStatement')->makePartial();
        $stMock->shouldReceive('execute');
        $stMock->shouldReceive('fetchAll')->andReturn([$license]);
        $dbMock->shouldReceive('prepare')->andReturn($stMock);


        $licenseRepository = new LicenseRepository($dbMock);

        $this->assertEquals([$license], $licenseRepository->getAll());
    }

    public function testGetById()
    {
        $faker = Faker\Factory::create();

        $license = new License();
        $license->setName($faker->name);
        $license->setPrice($faker->randomFloat(2));
        $license->setWebsiteAllowance($faker->randomDigitNotNull());


        $dbMock = m::mock('\Lib\Database')->makePartial();
        $stMock = m::mock('PDOStatement')->makePartial();
        $stMock->shouldReceive('execute');
        $stMock->shouldReceive('fetch')->andReturn($license);
        $dbMock->shouldReceive('prepare')->andReturn($stMock);


        $licenseRepository = new LicenseRepository($dbMock);

        $this->assertEquals($license, $licenseRepository->getById($faker->randomDigitNotNull()));
    }
}
