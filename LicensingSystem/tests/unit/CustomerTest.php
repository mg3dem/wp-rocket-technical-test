<?php

use \lib\License\Repository;
use \lib\Customer\Customer;
use \lib\Customer\CustomerRepository;
use Mockery as m;

class CustomerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    // tests
    public function testCustomer()
    {

        $faker = Faker\Factory::create();

        $customer = new Customer();

        //test name
        $name = $faker->name;
        $customer->setName($name);
        $this->assertEquals($name, $customer->getName());

        //Test email
        $email = $faker->email();
        $customer->setEmail($email);
        $this->assertEquals($email, $customer->getEmail());

        //Test Password
        $password = $faker->password();
        $customer->setPassword($password);

        $reflector = new \ReflectionClass($customer);
        $reflector_property = $reflector->getProperty('password');
        $reflector_property->setAccessible(true);

        $hashedPassword = $reflector_property->getValue($customer);
        $this->assertTrue(password_verify($password, $hashedPassword));

        //Test License
        $licenseId = $faker->randomDigitNotNull();
        $customer->setLicense($licenseId);
        $crMock = m::mock('\Lib\Customer\LicenseRepository');
        $crMock->shouldReceive('getById');
    }


    public function testToArray()
    {
        $faker = Faker\Factory::create();
        $customer = new Customer();

        $customerArray = [
            'name' => $faker->name,
            'email' => $faker->email()
        ];

        $customer->setName($customerArray['name']);
        $customer->setEmail($customerArray['email']);

        $this->assertIsArray($customer->toArray());
        $this->assertArraySubset($customerArray, $customer->toArray());

    }
}