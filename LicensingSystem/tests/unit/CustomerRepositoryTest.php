<?php

use lib\Database;
use \lib\Customer\Customer;
use \lib\Customer\CustomerRepository;
use Mockery as m;

class CustomerRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    // tests
    public function testGetAll()
    {
        $faker = Faker\Factory::create();
        $customer = new Customer();

        $customer->setName($faker->name);
        $customer->setEmail($faker->email());
        $customer->setPassword($faker->password());
        $customer->setLicense($faker->randomDigitNotNull());


        $dbMock = m::mock('\Lib\Database')->makePartial();
        $stMock = m::mock('PDOStatement')->makePartial();
        $stMock->shouldReceive('execute');
        $stMock->shouldReceive('fetchAll')->andReturn([$customer]);
        $dbMock->shouldReceive('prepare')->andReturn($stMock);


        $customerRepository = new CustomerRepository($dbMock);

        $this->assertEquals([$customer], $customerRepository->getAll());
    }

    public function testGetById()
    {
        $faker = Faker\Factory::create();
        $customer = new Customer();

        $customer->setName($faker->name);
        $customer->setEmail($faker->email());
        $customer->setPassword($faker->password());
        $customer->setLicense($faker->randomDigitNotNull());


        $dbMock = m::mock('\Lib\Database')->makePartial();
        $stMock = m::mock('PDOStatement')->makePartial();
        $stMock->shouldReceive('execute');
        $stMock->shouldReceive('fetch')->andReturn($customer);
        $dbMock->shouldReceive('prepare')->andReturn($stMock);


        $customerRepository = new CustomerRepository($dbMock);


        $this->assertEquals($customer, $customerRepository->getById($faker->randomDigitNotNull()));
    }

    public function testUpdate()
    {
        $faker = Faker\Factory::create();
        $customer = new Customer();

        $customer->setName($faker->name);
        $customer->setEmail($faker->email());
        $customer->setPassword($faker->password());
        $customer->setLicense($faker->randomDigitNotNull());


        $dbMock = m::mock('\Lib\Database')->makePartial();
        $stMock = m::mock('PDOStatement')->makePartial();
        $dbMock->shouldReceive('prepare')->andReturn($stMock);
        $stMock->shouldReceive('execute')->with($customer->toArray())->andReturnTrue();

        $customerRepository = new CustomerRepository($dbMock);
        $customerRepository->update($customer);
    }
}