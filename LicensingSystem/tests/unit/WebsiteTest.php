<?php

use \lib\Website\Website;

class WebsiteTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    // tests
    public function testWebsite()
    {

        $faker = Faker\Factory::create();
        $website = new Website();

        $url = $faker->url;
        $website->setUrl($url);
        $this->assertEquals($url, $website->getUrl());

        $customerId = $faker->randomDigitNotNull();
        $website->setCustomer($customerId);
        $this->assertEquals($customerId, $website->getCustomer());
    }

    public function testToArray()
    {
        $faker = Faker\Factory::create();
        $websiteArray = [
            'url' => $faker->url,
            'customer' => $faker->randomDigitNotNull()
        ];
        $website = new Website($websiteArray['url'], $websiteArray['customer']);

        $this->assertIsArray($website->toArray());
        $this->assertArraySubset($websiteArray, $website->toArray());
    }
}