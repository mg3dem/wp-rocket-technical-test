<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * Index page, working as first screen and user login, Login isn't implemented,
 * but choosing an user from the list will load the user's info.
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

require 'init.php';

use lib\Customer\CustomerRepository;

$customerRepository = CustomerRepository::Instance();

$customers = $customerRepository->getAll();
?>

<?php require 'layout/header.php'; ?>
<div class="container">
    <ul class="list-group">
        <?php foreach ($customers as $customer) { ?>
            <a href="main.php?user=<?php echo $customer->getId(); ?>" class="list-group-item list-group-item-action">
                <h5><?php echo $customer->getName(); ?></h5>
            </a>
        <?php } ?>
    </ul>
</div>
<?php require 'layout/footer.php'; ?>
