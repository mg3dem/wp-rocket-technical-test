<?php
/**
 * WP-Rocket Technical test - Licensing system
 *
 * Init page - Autoloading setup and session initialization
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

define('BASE_PATH', realpath(dirname(__FILE__)));

include BASE_PATH . '/vendor/autoload.php';

//var_dump($_SESSION);
session_start();

if ($_SESSION['customerId']) {
    $customerId = $_SESSION['customerId'];
}