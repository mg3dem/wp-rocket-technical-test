<?php

/**
 * WP-Rocket Technical test - Code review
 *
 * In our account page, we display the list of websites of a customer and we wanted
 * to display a website detail which displays the url and thumbnail of the website.
 * A fellow developer wrote this piece of code to do the job and is asking you to
 * review it.
 *
 * @author Miguel Gallardo <mg.prg.arg@gmail.com>
 */

require 'path/out/of/webroot/constants.php';

$dbConnection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if ($dbConnection->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

$customerId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

if ($result = $mysqli->query("SELECT * FROM websites WHERE id=" . $customerId)) {
    while ($website = $result->fetch_assoc()) {
        ?>
        <div>
            <h3><?php echo $website["url"]; ?></h3>
            <img src="<?php echo $website["thumbnail"]; ?>"/>
            <p>You are viewing <a href="website_details and .php?id=<?php echo $website['id']; ?>">This website</a></p>
        </div>
        <?php
    }
    $result->close();
} else { ?>
    <div class="error">Websites not found</div>
<?php } ?>